.PHONY: up do ex

SHELL = /bin/sh

CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

export CURRENT_UID
export CURRENT_GID

rebuild:
	docker build --no-cache -t ulco-fizz .

build:
	docker build -t ulco-fizz .

up:
	docker run -it -d --env-file ./docker/xdebug.env --name ulco-fizz -v $(shell pwd):/app -v $(shell pwd)/docker/xdebug.ini:/usr/local/etc/php/conf.d/docker-php-ext-xdebug-20.ini:ro -w=/app ulco-fizz
	docker exec -it ulco-fizz sh ./docker/setup-xdebug.sh
	docker exec -it ulco-fizz composer install

up-w:
	docker run -it -d --env-file ./docker/xdebug.env --name ulco -v ${CURDIR}:/app -v ${CURDIR}/docker/xdebug.ini:/usr/local/etc/php/conf.d/docker-php-ext-xdebug-20.ini:ro -w=/app ulco-fizz
	docker exec -it ulco-fizz sh ./docker/setup-xdebug.sh
	docker exec -it ulco-fizz composer install

do:
	docker rm -vf ulco-fizz

ex:
	docker exec -u $(CURRENT_UID):$(CURRENT_GID) -it ulco-fizz sh
